Simulateur d'optique géométrique - Mini-projet d'Algorithmique et programmation - semestre 4

--------------------------------------------------------------------------------------------

Sont compris dans ce dossier :

- le compte-rendu du mini-projet, fichier nommé MMoreau_VBeatrix_Compte_rendu.pdf, contenant
toutes les informations relatives au projet et à notre programme ;

- la classe permettant l'exécution du programme, fichier nommé MainSimulationOptique.java ;

- la classe qui gère l'interface graphique, fichier nommé FenetrePrincipale.java ;

- la classe qui gère le dessin des objets graphiques, fichier nommé PanelDessin.java ;

- la classe abstraite qui gère les objets optiques, c'est-à-dire les lentilles, les objets
à l'infini et les objets à distance finie, fichier nommé ObjetOptique.java ;

- la classe dérivée de ObjetOptique.java qui gère les objets sources situés à distance finie
ou à l'infini, fichier nommé Objet.java ;

- la classe dérivée de ObjetOptique.java qui gère les lentilles minces convergentes et
divergentes, fichier nommé LentilleMince.java.

--------------------------------------------------------------------------------------------

2019 par Merlin MOREAU et Vincent BEATRIX