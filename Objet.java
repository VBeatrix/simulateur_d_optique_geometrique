/**
 * Cette classe définit les objets (objets et images) et les objets situés à l'infini.
 */
import java.util.ArrayList;

public class Objet extends ObjetOptique implements Comparable<Objet>{

    public double taille ; // indique la taille de l'objet : elle peut être positive ou négative. Pour un objet infini, la taille est une distance particulière qui correspond à un triangle rectange de base 1 (Annexe 1)
    public boolean infini; // true si on a un objet infini, false sinon
    
    /** Constructeur pour un objet réel
     * @param p la position, t la taille
     */
    public Objet (double p, double t) {
        super(p) ;
        this.taille = t ;
        this.infini = false;
    }
    /** Constructeur utilisé pour un objet infini
     * @param t est un angle en degré
     */
    public Objet (double angle){
        super();
        this.taille = Math.tan(angle*(2*Math.PI)/360);
        this.infini = true;
    }
    /** Constructeur par défaut si aucun paramètre n'est donné
     */
    public Objet(){
        super();
        this.taille = 0;
        this.infini = false;
    }

    /** Méthode calculImage
     * Cette méthode permet de calculer la position du plan image d'un objet en fonction de différentes lentilles
     * @param arrayList<LentilleMince> listeLentille
     * @return Objet image
     */
    public Objet calculImage(ArrayList<LentilleMince> listeLentille){
        Objet image = new Objet(); //Image qui résulte du sytème obtique appliqué
		image.position = this.position;
		image.taille = this.taille;
		image.infini = this.infini;

        for(int i = 0; i<listeLentille.size();i++){ // Boucle qui permet de parcourir toutes les lentilles de la liste
            if(image.infini != true && image.position != listeLentille.get(i).position-listeLentille.get(i).distanceFocale){ //Si l'objet n'est pas infini et n'est pas situé au point focal objet
				double OA = image.position-listeLentille.get(i).position; //Position de l'objet par rapport à la lentille
				double f = listeLentille.get(i).distanceFocale; //Distance focale de la lentille traitée
				image.position = (OA*f)/(OA+f); //Application des relations de conjugaison, position par rapport à la lentille
				image.taille = (image.position) * image.taille / OA; //Application de la formule du grandissement
				image.position = image.position + listeLentille.get(i).position; //On passe d'une position définie par la lentille à une position définie par l'origine du repère.
            }else if (image.infini == true){ //Si l'objet est infini
				image.position = listeLentille.get(i).distanceFocale + listeLentille.get(i).position; //La position = la distance focale de la lentille
				image.taille = -image.taille * listeLentille.get(i).distanceFocale; // La taille est déterminée grâce à la norme donnée sur la taille pour les objets infinis (Annexe 1)
				image.infini = false; //L'image n'est pas à l'infini
			}else{ //L'objet est situé sur le point focal objet de la lentille
				image.infini = true; //L'image est à l'infini
				image.taille = -image.taille / listeLentille.get(i).distanceFocale; //Sa taille est définie comme pour un objet infini
			}  
        }
        return image;
    }
    /** Méthode calculImage
     * Cette méthode permet de calculer la position du plan image d'un objet en fonction de différentes lentilles
     * @param LentilleMince l
     * @return Objet image
     */
    public Objet calculImage(LentilleMince l){
        Objet image = new Objet(); //Image qui résulte du sytème optique appliqué
		image.position = this.position;
		image.taille = this.taille;
		image.infini = this.infini;
            if(image.infini != true && image.position != l.position-l.distanceFocale){//Si l'objet n'est pas infini et n'est pas situé au point focal objet
				double OA = image.position-l.position; //Position de l'objet par rapport à la lentille
				double f = l.distanceFocale; //Distance focale de la lentille traitée
				image.position = (OA*f)/(OA+f); //Application des relations de conjugaison, position par rapport à la lentille
				image.taille = (image.position) * image.taille / OA; //Application de la formule du grandissement
				image.position = image.position + l.position; //On passe d'une position définie par la lentille à une position définie par l'origine du repère.
            }else if (image.infini == true){ //Si l'objet est infini
				image.position = l.distanceFocale + l.position; //La position = la distance focale de la lentille
				image.taille = -image.taille * l.distanceFocale; // La taille est déterminée grâce à la norme donnée sur la taille pour les objets infini (Annexe 1)
				image.infini = false; //L'image n'est pas à l'infini
			}else{ //L'objet est situé sur le point focal objet de la lentille
				image.infini = true; //L'image est à l'infini
				image.taille = -image.taille / l.distanceFocale; //Sa taille est définie comme pour un objet infini
			}  
        return image;
    }
        
    /**
     * Méthode toString
     */
    public String toString(){
        String s = ("Taille = " + taille + "\nPosition = " + position + "\nInfini = " + infini) ;
        return s ;
    }
    
    //Méthode equals, permet de déterminer si deux objets sont identiques.
    public boolean equals(Objet O2){
        return ((this.position == O2.position) && (this.taille == O2.taille));
    }
    
    //Méthode compareTo permet de comparer deux objets pour ensuite les trier.
    public int compareTo(Objet O2){
    
        //Critère principal sur la position
        if (this.position < O2.position){
            return -1;
        }else if (this.position > O2.position){
            return 1;
        }else{
			//Critère secondaire sur la taille
            if (this.taille < O2.taille){
                return -1;
            }else if (this.taille > O2.taille){
                return 1;
            }else{
                return 0;
            }
        }
    }
}
