/*
 * Cette classe s'occupe des lentilles minces
 */
public class LentilleMince extends ObjetOptique implements Comparable<LentilleMince>{

    public double distanceFocale ; // indique la distance focale de la lentille : peut être négative (lentille divergente) ou positive (lentille convergente).
    
    /**
     * Constructeur
     * @param dF la distance focale, p la position
     */
    public LentilleMince(double dF, double p){
		distanceFocale = dF;
		position = p;
	}
	
    //Méthode equals, permet de déterminer si deux lentilles minces sont identiques.
    public boolean equals(LentilleMince L2){
        return ((this.position == L2.position) && (this.distanceFocale == L2.distanceFocale));
    }
    
    //Méthode compareTo, permet de comparer deux lentilles minces pour ensuite les trier.
    public int compareTo(LentilleMince L2){    
        // Critère principal sur la position
        if (this.position < L2.position){
            return -1;
        }else if (this.position > L2.position){
            return 1;
        }else{
            // Si la position est la même, on regarde la distance focale.            
            if (this.distanceFocale < L2.distanceFocale){
                return -1;
            }else if (this.distanceFocale > L2.distanceFocale){
                return 1;
            }else{
                return 0;
            }
        }
    }
	
}
