/*
 * Cette classe est la classe mère pour tous les objets optiques à manipuler.
 */
public abstract class ObjetOptique {

    public double position ; // indique la position de l'objet optique sur l'axe optique

    /** Constructeur avec le paramètre de position
     * @param double p la position
     */
    public ObjetOptique (double p) {
        position = p ;
    }
    /** Constructeur par défaut si aucun paramètre n'est donné
     */
    public ObjetOptique(){
        position = 0;
    }
    
}
