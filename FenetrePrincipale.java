/**
 * Cette classe gère la fenêtre principale avec laquelle l'utilisateur interagit.
 */

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class FenetrePrincipale extends JFrame implements ActionListener, MouseListener{
	
	// Déclaration de tous les JPanel de la fenêtre
	
	private JPanel pPrincipal; //Panel principal qui contient les autres panels
    private JPanel pHaut; //Panel situé en haut, il contient les boutons
    private PanelDessin pCentre; //Panel situé au centre, il contient le tracé
    private JPanel pBas; // Panel situé en bas, il contient des informations sur le tracé
    
    // Déclaration de tous les boutons de la fenêtre
    
    private JButton bObjet; //Bouton pour ajouter un objet
    private JButton bInfini; //Bouton pour ajouter un objet infini
    private JButton bLentille; //Bouton pour ajouter une lentille
    private JButton bEchelle; //Bouton pour modifier l'échelle du tracé
    private JButton bDessin; //Bouton pour dessiner les éléments ajoutés au tracé
    private JButton bEffacer; //Bouton pour effacer tout le dessin.
    
    private JButton objetOK; //Bouton pour valider les données d'un objet
    private JButton infiniOK; //Bouton pour valider les données d'un objet infini
    private JButton lentilleOK; //Bouton pour valider les données d'une lentille
    private JButton echelleOK; //Bouton pour valider les données de l'échelle
    
    // Déclaration de tous les JPanels de la fenêtre
    
    private JLabel lEchelleH; //Label au-dessus du champ de texte pour l'échelle horizontale
    private JLabel lEchelleV; //Label au-dessus du champ de texte pour l'échelle verticale
    private JLabel lTaille; //Label au-dessus du champ de texte pour la taille d'un objet
    private JLabel lPosition; //Label au-dessus du champ de texte pour la position d'un objet
    private JLabel lAngle; //Label au-dessus du champ de texte pour l'angle d'un objet infini
    private JLabel lFocale; //Label au-dessus du champ de texte pour la focale d'une lentille
    private JLabel lPixelH; //Label pour l'affichage de l'échelle horizontale 
    private JLabel lPixelV; //Label pour l'affichage de l'échelle verticale
    
    // Déclaration de tous les JTextField de la fenêtre
    
    private JTextField taille; //Champ de texte pour saisir la taille d'un objet
    private JTextField position; //Champ de texte pour saisir la position d'un objet optique (lentille ou objet)
    private JTextField angle; //Champ de texte pour saisir l'angle d'incidence en degré des rayons d'un objet infini
    private JTextField focale; //Champ de texte pour saisir la focale d'une lentille
    private JTextField pixelH; //JTextField pour saisir l'échelle horizontale
    private JTextField pixelV; //JTextField pour saisir l'échelle verticale
    
    // Déclaration de toutes les variables numériques de la fenêtre
    
    private int tailleInt; //Variable qui permet de récupérer la valeur de la taille saisie par l'utilisateur
	private int positionInt; //Variable qui permet de récupérer la valeur de la position saisie par l'utilisateur
    private int angleInt; //Variable qui permet de récupérer la valeur de l'angle d'incidence d'un objet infini saisie par l'utilisateur
    private int focaleInt; //Variable qui permet de récupérer la valeur de la focale d'une lentille saisie par l'utilisateur
    private int echelleIntH; //Variable qui permet de récupérer la valeur de l'échelle horizontale saisie par l'utilisateur
    private int echelleIntV; //Variable qui permet de récupérer la valeur de l'échelle verticale saisie par l'utilisateur
    private int pxx; //Echelle en pixel horizontale
    private int pxy; //Echelle en pixel verticale
    private int positionObjetModif = -1; //Variable qui permet de déterminer la position d'un objet dans une zone prédéfinie lors d'un clic. S'il n'y a pas de clic la valeur vaut -1.
    private int positionLentilleModif = -1; //Variable qui permet de déterminer la position d'une lentille dans une zone prédéfinie lors d'un clic. S'il n'y a pas de clic la valeur vaut -1.
    
    // Déclaration de toutes les ArrayList de la fenêtre
    
    private ArrayList<LentilleMince> listeLentille; //Liste contenant toutes les lentilles
    private ArrayList<Objet> listeObjet; //Liste contenant tous les objets
    
    /** 
	* Constructeur
    */
	public FenetrePrincipale() {
		super();
		
		// Initialisation de la fenêtre
		
		this.setTitle("Simulateur d'optique geometrique");
		this.setSize(900, 450);
		this.setLocation(0, 0);
		
		//Initialisation des ArrayList
		
		listeObjet = new ArrayList<Objet>();
		listeLentille = new ArrayList<LentilleMince>();
		
		//Initialisation du JPanel principal
		
        pPrincipal = new JPanel();
        pPrincipal.setLayout(null);
       
        //Initialisation du JPanel supérieur
        
        pHaut = new JPanel();
        pHaut.setLayout(null);
        pHaut.setBackground(new Color(49, 140, 231));
        
        //Initialisation du JPanel central à l'aide d'un PanelDessin
        
        pCentre = new PanelDessin();
		pCentre.setLayout(null);
		
        //Initialisation du JPanel inféieur
        
        pBas = new JPanel();
        pBas.setLayout(null);
        pBas.setBackground(new Color(49, 140, 231));
        
        // Initialisation des valeurs de l'échelle et application à pCentre
        
        this.pxx = 10; //L'échelle par défaut est fixée à 10px pour 1cm.
        this.pxy = 10; //L'échelle par défaut est fixée à 10px pour 1cm.
        
        //Initialisation des JLabel permettant l'affichage de l'échelle

        lEchelleH = new JLabel(this.pxx+"px/1cm (X)");
        lEchelleH.setForeground(Color.white);
        lEchelleH.setForeground(Color.white);
        
        lEchelleV = new JLabel(this.pxy+"px/1cm (Y)");
        lEchelleV.setForeground(Color.white);
        lEchelleV.setForeground(Color.white);
        
        //Initialisation des JButton du JPanel supérieur
        
        bObjet = new JButton("Objet");
        bObjet.setBackground(new Color(206, 206, 206));
		bObjet.setForeground(Color.black);
		
        bInfini = new JButton("Objet a l'infini");
        bInfini.setBackground(new Color(206, 206, 206));
		bInfini.setForeground(Color.black);
		
        bLentille = new JButton("Lentille");
        bLentille.setBackground(new Color(206, 206, 206));
		bLentille.setForeground(Color.black);
		
        bEchelle = new JButton("Echelle");
        bEchelle.setBackground(new Color(206, 206, 206));
		bEchelle.setForeground(Color.black);
		
        bDessin = new JButton("Dessiner");
        bDessin.setBackground(new Color(206, 206, 206));
		bDessin.setForeground(Color.black);
		
        bEffacer = new JButton("Effacer");
        bEffacer.setBackground(new Color(206, 206, 206));
		bEffacer.setForeground(Color.black);
        
        //Initialisation des JLabel au-dessus des JTextField permettant de saisir les différentes données
        
        lTaille = new JLabel("Taille ? (nombre entier positif ou negatif)");
        lTaille.setForeground(Color.white);
        lTaille.setVisible(false);
        
        lAngle = new JLabel("Angle d'incidence ? (nombre entier positif ou negatif)");
        lAngle.setForeground(Color.white);
        lAngle.setVisible(false);
        
        lFocale = new JLabel("Distance focale ? (nombre entier positif ou negatif)");
        lFocale.setForeground(Color.white);
        lFocale.setVisible(false);
        
        lPosition = new JLabel("Position ? (nombre entier positif ou negatif)");
        lPosition.setForeground(Color.white);
        lPosition.setVisible(false);
        
        lPixelH = new JLabel("... px/cm X (entier)");
        lPixelH.setForeground(Color.white);
        lPixelH.setVisible(false);
        
        lPixelV = new JLabel("... px/cm Y (entier)");
        lPixelV.setForeground(Color.white);
        lPixelV.setVisible(false);
        
        //Initialisation des JTextField permettant de saisir les différentes données
        
		taille = new JTextField();
		taille.setVisible(false);
        
        angle = new JTextField();
        angle.setVisible(false);
        
        focale = new JTextField();
        focale.setVisible(false);
        
		position = new JTextField();
		position.setVisible(false);
        
        pixelH = new JTextField();
        pixelH.setVisible(false);
        
        pixelV = new JTextField();
        pixelV.setVisible(false);
        
        //Initialisation des JButton permettant de valider les données des différents éléments (Objet, Lentille, Objet Infini, Echelle)
        
		objetOK = new JButton("OK");
		objetOK.setVisible(false);
		objetOK.setBackground(Color.red);
		objetOK.setForeground(Color.white);
		
        infiniOK = new JButton("OK");
        infiniOK.setVisible(false);
        infiniOK.setBackground(Color.red);
		infiniOK.setForeground(Color.white);
        
        lentilleOK = new JButton("OK");
        lentilleOK.setVisible(false);
        lentilleOK.setBackground(Color.red);
		lentilleOK.setForeground(Color.white);
        
        echelleOK = new JButton("OK");
        echelleOK.setVisible(false);
        echelleOK.setBackground(Color.red);
		echelleOK.setForeground(Color.white);

        //Ajout des ActionListener nécessaires sur les différents JButton
        
		bObjet.addActionListener(this);
        bInfini.addActionListener(this);
        bLentille.addActionListener(this);
        bEchelle.addActionListener(this);
		bDessin.addActionListener(this);
        bEffacer.addActionListener(this);
		objetOK.addActionListener(this);
        infiniOK.addActionListener(this);
        lentilleOK.addActionListener(this);
        echelleOK.addActionListener(this);
        
        //Ajout du MouseListener sur le JPanel central pour pouvoir intéragir avec
        
        pCentre.addMouseListener(this);
        
		//Ajout des différents éléments au JPanel supérieur
		
		pHaut.add(bObjet);
        pHaut.add(bInfini);
        pHaut.add(bLentille);
        pHaut.add(bEchelle);
        pHaut.add(bDessin);
        pHaut.add(bEffacer);
        pHaut.add(taille);
        pHaut.add(angle);
        pHaut.add(focale);
        pHaut.add(pixelH);
        pHaut.add(pixelV);
        pHaut.add(position);
        pHaut.add(objetOK);
        pHaut.add(infiniOK);
        pHaut.add(lentilleOK);
        pHaut.add(echelleOK);
        pHaut.add(lTaille);
        pHaut.add(lAngle);
        pHaut.add(lFocale);
        pHaut.add(lPixelH);
        pHaut.add(lPixelV);
        pHaut.add(lPosition);
        
		//Ajout des différents éléments au JPanel inférieur
		
        pBas.add(lEchelleV);
        pBas.add(lEchelleH);
        
		//Ajout des JPanel secondaires au JPanel principal
		
		pPrincipal.add(pHaut);
		pPrincipal.add(pCentre);
        pPrincipal.add(pBas);
        
        // Ajout du JPanel principal à la fenêtre
     
		this.add(pPrincipal);
		
        this.setVisible(true); //On rend la fenêtre visible
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //On règle le fait que fermer la fenêtre arrête le programme
        
        Timer mt = new Timer(100,this); //On crée un timer mt de fréquence 10Hz
		mt.start(); //On démarre le timer mt
	}
	
    /**
    * Méthode qui masque les boutons principaux du JPanel supérieur
    */
    public void setFalsePHaut (){
        bObjet.setVisible(false);
        bInfini.setVisible(false);
        bLentille.setVisible(false);
        bEchelle.setVisible(false);
        bDessin.setVisible(false);
        bEffacer.setVisible(false);
    }
    
    /**
	* Méthode qui affiche les boutons principaux du JPanel supérieur
    */
    public void setTruePHaut (){
        bObjet.setVisible(true);
        bInfini.setVisible(true);
        bLentille.setVisible(true);
        bEchelle.setVisible(true);
        bDessin.setVisible(true);
        bEffacer.setVisible(true);
    }
    
    /**
	* Méthode qui masque les éléments de réglage d'un objet du JPanel supérieur
    */
    public void setFalseObjet(){
        taille.setVisible(false);
        position.setVisible(false);
        objetOK.setVisible(false);
        lTaille.setVisible(false);
        lPosition.setVisible(false);
    }
    
    /**
	* Méthode qui affiche les éléments de réglage d'un objet du JPanel supérieur
    */
    public void setTrueObjet(){
        taille.setVisible(true);
        position.setVisible(true);
        objetOK.setVisible(true);
        lTaille.setVisible(true);
        lPosition.setVisible(true);
    }
    
    /**
	* Méthode qui masque les éléments de réglage d'un objet infini du JPanel supérieur
    */
    public void setFalseInfini(){
        infiniOK.setVisible(false);
        angle.setVisible(false);
        lAngle.setVisible(false);
    }
    
    /**
	* Méthode qui affiche les éléments de réglage d'un objet infini du JPanel supérieur
    */
    public void setTrueInfini(){
        infiniOK.setVisible(true);
        lentilleOK.setVisible(true);
        echelleOK.setVisible(true);
        angle.setVisible(true);
        lAngle.setVisible(true);
    }
    
    /**
	* Méthode qui masque les éléments de réglage d'une lentille du JPanel supérieur
    */
    public void setFalseLentille(){
        position.setVisible(false);
        lentilleOK.setVisible(false);
        focale.setVisible(false);
        lPosition.setVisible(false);
        lFocale.setVisible(false);
    }
    
    /**
	* Méthode qui affiche les éléments de réglage d'une lentille du JPanel supérieur
    */
    public void setTrueLentille(){
        position.setVisible(true);
        lentilleOK.setVisible(true);
        focale.setVisible(true);
        lPosition.setVisible(true);
        lFocale.setVisible(true);
    }
    
    /**
	* Méthode qui masque les éléments de réglage de l'échelle du JPanel supérieur
    */
    public void setFalseEchelle(){
        echelleOK.setVisible(false);
        pixelH.setVisible(false);
        lPixelH.setVisible(false);
        pixelV.setVisible(false);
        lPixelV.setVisible(false);
    }
    
    /**
	* Méthode qui affiche les éléments de réglage de l'échelle du JPanel supérieur
    */
    public void setTrueEchelle(){
        echelleOK.setVisible(true);
        pixelH.setVisible(true);
        lPixelH.setVisible(true);
        pixelV.setVisible(true);
        lPixelV.setVisible(true);
    }
    
    /**
	* Méthode qui permet l'ajout d'un objet dans pCentre
    */
    public void addObjetDessin(){
		for(Objet a : listeObjet){
			pCentre.addObjet(a);
		}
	}
	
	/**
	* Méthode qui renvoie true si une chaîne de caractères ne contient que des chiffres, false sinon.
    */
	public boolean verifierChiffre(String a){
		boolean sansChiffre = true;
		int i = 0;
		while(i<a.length() && sansChiffre == true){
			if((int)a.charAt(i)<48 || (int)a.charAt(i)>57){
				sansChiffre = false;
			}
			i++;
		}
		return sansChiffre;
	}
	
    /**
	* Méthode qui permet l'ajout d'une lentille dans pCentre
    */
	public void addLentilleDessin(){
		for(LentilleMince b : listeLentille){
			pCentre.addLentille(b);
		}
	}
	
	/**
	* Méthodes obligatoires suite à l'implémentation de MouseListener
    */
    public void mouseExited (MouseEvent e){
	}
	public void mouseEntered (MouseEvent e){
	}
	public void mouseReleased(MouseEvent e){
	}
	public void mousePressed (MouseEvent e){
	}
	
	/**
	* Méthode active suite à un event de type MouseEvent
    */
	public void mouseClicked (MouseEvent e){
		
		int posXClick = e.getX(); // Variable enregistrant la coordonnée x d'un clic de souris
		
		Objet a = new Objet(pCentre.getWidth(),0); // Initialisation d'un objet a permettant d'enregistrer l'objet le plus proche sur la droite du lieu du clic de la souris ; l'objet est placé le plus à droite possible
		LentilleMince b = new LentilleMince(0,pCentre.getWidth()); // Initialisation d'une lentille b permettant d'enregistrer l'objet le plus proche sur la droite du lieu du clic de la souris ; la lentille est placée le plus à droite
		
		// On trie les deux ArrayList pour avoir les objets et les lentilles dans l'ordre
		Collections.sort(listeObjet);
		Collections.sort(listeLentille);
		
		positionObjetModif = 0; // On initialise une variable permettant de déterminer la position dans la liste de l'objet désiré
		positionLentilleModif = 0; // On initialise une variable permettant de déterminer la position dans la liste de la lentille désirée
		boolean existe = false; // On initialise un boolean permettant de dire s'il y a bien un objet ou une lentille dans une largeur de 5 px après le clic
		
		// Boucle conditionnelle permettant de déterminer la position de l'objet désiré dans la liste
		while(positionObjetModif<listeObjet.size() && existe==false){
			// Conditions qui sont vérifiées si l'objet est bien l'objet cliqué
			if(pxx*listeObjet.get(positionObjetModif).position >= posXClick && pxx*listeObjet.get(positionObjetModif).position <= (posXClick+5) && listeObjet.get(positionObjetModif).infini == false){
				existe = true;
				a = listeObjet.get(positionObjetModif);
			}
			positionObjetModif++;
		}
		
		existe = false; // On définit existe false pour refaire la même procédure qu'auparavant mais avec la lentille
		
		// Boucle conditionnelle permettant de déterminer la position de la lentille désirée dans la liste
		while(positionLentilleModif<listeLentille.size() && existe==false){
			// Conditions qui sont vérifiées si la lentille est bien la lentille cliquée
			if(pxx*listeLentille.get(positionLentilleModif).position >= posXClick && pxx*listeLentille.get(positionLentilleModif).position <= posXClick+5){
				existe = true;
				b = listeLentille.get(positionLentilleModif);
			}
			positionLentilleModif++;
		}
		
		// Condition qui permet de déterminer qui de la lentille ou de l'objet est le plus proche du clic et qui permet d'afficher si nécessaire les bons éléments
		if(a.position<b.position && a.position != 0){
			setFalsePHaut();
			taille.setText(""+(int)a.taille);
			position.setText(""+(int)a.position);
			setTrueObjet();
		}
		else if(a.position>b.position && b.position != 0){
			setFalsePHaut();
			focale.setText(""+(int)b.distanceFocale);
			position.setText(""+(int)b.position);
			setTrueLentille();
		}
	}

    /**
	* Méthode active suite à un event de type ActionEvent
    */
    public void actionPerformed (ActionEvent e) {
        if(e.getSource() == bObjet){  //Si on appuie sur le bouton principal bObjet, on affiche les éléments de paramétrage d'un objet
            setFalsePHaut();
            positionObjetModif = -1;
            setTrueObjet();
        } else if(e.getSource() == bInfini){ //Si on appuie sur le bouton principal bInfini, on affiche les éléments de paramétrage d'un objet infini.
            setFalsePHaut();
            setTrueInfini();
        } else if(e.getSource() == bLentille){ //Si on appuie sur le bouton principal bLentille, on affiche les élèments de paramétrage d'une lentille.
            setFalsePHaut();
            positionLentilleModif = -1;
            setTrueLentille();
        } else if(e.getSource() == bEchelle){ //Si on appuie sur le bouton principal bEchelle, on affiche les éléments de paramétrage de l'échelle
            setFalsePHaut();
            setTrueEchelle();
        } else if(e.getSource() == bDessin) { //Si on appuie sur le bouton principal bDessin, on dessine les nouveaux éléments dans le panel central.
            pCentre.effacerDessin();
			addObjetDessin();
			addLentilleDessin();
			pCentre.repaint();
        } else if(e.getSource() == bEffacer) { //Si on appuie sur le bouton principal bEffacer, on efface le tracé du panel central.
			pCentre.effacerDessin();
			listeLentille.clear();
			listeObjet.clear();
        } else if(e.getSource() == objetOK){ //Si on appuie sur les boutons objetOK, on traite les données rentrées dans les JTextField associés au paramétrage d'un objet
            setTruePHaut();
            setFalseObjet();
            if(taille.getText().length() != 0 && verifierChiffre(taille.getText()) == true && position.getText().length() != 0 && verifierChiffre(position.getText()) == true){ //Condition pour accepter les valeurs entrées par l'utilisateur (n'est pas vide et ne contient que des chiffres)
				//On récupere les valeurs saisies par l'utilisateur
				tailleInt= Integer.parseInt(taille.getText());
				positionInt= Integer.parseInt(position.getText());
				
				//Condition pour savoir si c'est un nouvel objet ou un objet qu'on veut modifier
				if(positionObjetModif == -1) {
					listeObjet.add(new Objet(positionInt,tailleInt));
				} else {
					listeObjet.get(positionObjetModif-1).position = positionInt;
					listeObjet.get(positionObjetModif-1).taille = tailleInt;
					pCentre.repaint();	
					}					
			}
			else if(positionObjetModif != -1){ //Condition pour supprimer un objet si un champ n'est pas valide quand on le modifie
				listeObjet.remove(positionObjetModif-1);
				pCentre.repaint();	
			}
			//On remet les JTextField vides
			taille.setText("");
			position.setText("");
        } else if(e.getSource() == infiniOK ){ //Si on appuie sur les boutons infiniOK, on traite les données rentrées dans les JTextField associés au paramétrage d'un objet infini
            setTruePHaut();
            setFalseInfini();
            if(angle.getText().length() != 0 && verifierChiffre(angle.getText()) == true){ //Condition pour accepter les valeurs saisies par l'utilisateur (n'est pas vide et ne contient que des chiffres)
				angleInt= Integer.parseInt(angle.getText());
				listeObjet.add(new Objet(angleInt));
			}
        } else if(e.getSource() == lentilleOK){ //Si on appuie sur les boutons lentilleOK, on traite les données saisies dans les JTextField associés au paramétrage d'une lentille
            setTruePHaut();
            setFalseLentille();
            //Condition pour accepter les valeurs saisies par l'utilisateur (n'est pas vide et ne contient que des chiffres)
            if(position.getText().length() != 0 && focale.getText().length() != 0 && verifierChiffre(position.getText()) == true && verifierChiffre(focale.getText()) == true){
				//On récupere les valeurs saisies par l'utilisateur
				positionInt= Integer.parseInt(position.getText());
				focaleInt= Integer.parseInt(focale.getText());
				//Condition pour savoir si c'est une nouvelle lentille ou une lentille déjà existante qu'on veut modifier
				if(positionLentilleModif == -1){
					listeLentille.add(new LentilleMince(focaleInt, positionInt));
				} else {
					listeLentille.get(positionLentilleModif-1).distanceFocale = focaleInt;
					listeLentille.get(positionLentilleModif-1).position = positionInt;
					pCentre.repaint();
				}
			}
			else if(positionLentilleModif != -1){ //Condition pour supprimer un objet si un champ n'est pas valide quand on le modifie
				listeLentille.remove(positionLentilleModif-1);
				pCentre.repaint();
			}
			//On remet les JTextField vides
			focale.setText("");
			position.setText("");
        } else if(e.getSource() == echelleOK){ //Si on appuie sur le bouton echelleOK, on traite les données saisies dans les JTextField associés au paramétrage de l'échelle
            //On récupère les valeurs saisies par l'utilisateur
            setTruePHaut();
            setFalseEchelle();
			if(pixelH.getText().length() != 0 && pixelV.getText().length() != 0 && verifierChiffre(pixelH.getText()) == true && verifierChiffre(pixelV.getText()) == true){ //Condition pour accepter les valeurs saisies par l'utilisateur (n'est pas vide et ne contient que des chiffres)
				echelleIntH= Integer.parseInt(pixelH.getText());
				echelleIntV= Integer.parseInt(pixelV.getText());
				this.pxx = echelleIntH;
				lEchelleH.setText(this.pxx+"px/1cm");
				pCentre.pxx = this.pxx;
            
				this.pxy = echelleIntV;
				lEchelleV.setText(this.pxy+"px/1cm");
				pCentre.pxy = this.pxy;
				pCentre.repaint();
			}
        } else { //En réponse au timer pour permettre de s'adapter à la taille de la fenêtre.
			
			//On règle la taille du JPanel pPrincipal
			
            pPrincipal.setSize(this.getWidth() - this.getInsets().left - this.getInsets().bottom, this.getHeight()- this.getInsets().top - this.getInsets().bottom);
            
            //Position et dimension du JPanel supérieur
            
            pHaut.setLocation(0,0);
            pHaut.setSize(pPrincipal.getWidth(),(int)(0.1*pPrincipal.getHeight()));
            
            //Position et dimension du JPanel central
            
            pCentre.setLocation(0,(int)(0.1*pPrincipal.getHeight()));
            pCentre.setSize(pPrincipal.getWidth(),(int)(0.8*pPrincipal.getHeight()));
            
            //Position et dimension du JPanel inférieur
            
            pBas.setLocation(0,(int)(0.9*pPrincipal.getHeight()));
            pBas.setSize(pPrincipal.getWidth(),(int)(0.1*pPrincipal.getHeight()));
            
            // Déclaration et initialisation paramétrant les dimensions d'un bouton en fonction des dimensions de la fenêtre
            
            int lButtonPHaut = (int)(pPrincipal.getWidth()/7);          //largeur d'un bouton
            int hButtonPHaut = (int)(0.045*pPrincipal.getHeight());     // hauteur d'un bouton
            int mButtonPHaut = (int)(0.005*pPrincipal.getHeight());     // marge avec les cotes inférieur et supérieur;
            int eButtonPHaut = (int)(pPrincipal.getWidth()*0.02);       // écart entre les boutons
            
            //Positions et dimensions des boutons principaux sur le JPanel supérieur
            
            bObjet.setBounds(eButtonPHaut, mButtonPHaut, lButtonPHaut, 2*hButtonPHaut); 
            bInfini.setBounds(lButtonPHaut + (eButtonPHaut*2), mButtonPHaut, lButtonPHaut, 2*hButtonPHaut);
            bLentille.setBounds((2*lButtonPHaut) + (eButtonPHaut*3), mButtonPHaut, lButtonPHaut, 2*hButtonPHaut);
            bDessin.setBounds((4*lButtonPHaut) + (eButtonPHaut*5), mButtonPHaut, lButtonPHaut, 2*hButtonPHaut);
            bEchelle.setBounds((3*lButtonPHaut) + (eButtonPHaut*4), mButtonPHaut, lButtonPHaut, 2*hButtonPHaut);
            bEffacer.setBounds((5*lButtonPHaut) + (eButtonPHaut*6), mButtonPHaut, lButtonPHaut, 2*hButtonPHaut);
            
            //Positions et dimensions des boutons OK sur le JPanel supérieur
            
            objetOK.setBounds((4*lButtonPHaut) + (eButtonPHaut*5), mButtonPHaut+hButtonPHaut, lButtonPHaut, hButtonPHaut);
            infiniOK.setBounds((4*lButtonPHaut) + (eButtonPHaut*5), mButtonPHaut+hButtonPHaut, lButtonPHaut, hButtonPHaut);
            lentilleOK.setBounds((4*lButtonPHaut) + (eButtonPHaut*5), mButtonPHaut+hButtonPHaut, lButtonPHaut, hButtonPHaut);
            echelleOK.setBounds((4*lButtonPHaut) + (eButtonPHaut*5), mButtonPHaut+hButtonPHaut, lButtonPHaut, hButtonPHaut);
            
            //Positions et dimensions de tous les JLabel sur le JPanel supérieur
            
            lTaille.setBounds(eButtonPHaut, mButtonPHaut, lButtonPHaut, hButtonPHaut) ;
            lAngle.setBounds(eButtonPHaut, mButtonPHaut, lButtonPHaut, hButtonPHaut);
            lFocale.setBounds(eButtonPHaut, mButtonPHaut, lButtonPHaut, hButtonPHaut);
            lPixelH.setBounds(eButtonPHaut, mButtonPHaut, lButtonPHaut, hButtonPHaut);
            lPixelV.setBounds(lButtonPHaut + (eButtonPHaut*2), mButtonPHaut, lButtonPHaut, hButtonPHaut);
            lPosition.setBounds(lButtonPHaut + (eButtonPHaut*2), mButtonPHaut, lButtonPHaut, hButtonPHaut);
            
            //Positions et dimensions de tous les JTextField sur le JPanel supérieur
            
            taille.setBounds(eButtonPHaut, mButtonPHaut+hButtonPHaut, lButtonPHaut, hButtonPHaut) ;
            angle.setBounds(eButtonPHaut, mButtonPHaut+hButtonPHaut, lButtonPHaut, hButtonPHaut);
            focale.setBounds(eButtonPHaut, mButtonPHaut+hButtonPHaut, lButtonPHaut, hButtonPHaut);
            pixelH.setBounds(eButtonPHaut, mButtonPHaut+hButtonPHaut, lButtonPHaut, hButtonPHaut);
            pixelV.setBounds(lButtonPHaut + (eButtonPHaut*2), mButtonPHaut + hButtonPHaut, lButtonPHaut, hButtonPHaut);
            position.setBounds(lButtonPHaut + (eButtonPHaut*2), mButtonPHaut+hButtonPHaut, lButtonPHaut, hButtonPHaut);
            
            //Positions et dimensions des JLabel qui indiquent l'échelle d'affichage sur le JPanel inférieur
            
            lEchelleH.setBounds((int)(pBas.getWidth()*0.01), (int)(pBas.getHeight()*0.01),pBas.getWidth(), pBas.getHeight());
            lEchelleV.setBounds(pBas.getWidth() - 100, (int)(pBas.getHeight()*0.01),pBas.getWidth(),pBas.getHeight());     
        }
        
    }    
}

