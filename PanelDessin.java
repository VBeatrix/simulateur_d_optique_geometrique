/**
 * Cette classe gère l'affichage des objets optiques dans le panneau principal.
 */
 
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.*;
import java.awt.event.*;
import javax.swing.Timer;
import java.util.*;

public class PanelDessin extends JPanel{
	
	public ArrayList <Objet> listeObjet; //Contient la liste des objets à dessiner
	public ArrayList <LentilleMince> listeLentille; //Liste des lentilles, utile dans la construction de l'image de chaque objet
	public int pxx; //Echelle horizontale
	public int pxy; //Echelle verticale
	
	/**
	 * Constructeur
	 */
	public PanelDessin(){
		super();
		listeObjet = new ArrayList<Objet>(); //Initialisation de listeObjet
		listeLentille = new ArrayList<LentilleMince>(); //Initialisation de listeLentille
		this.pxx = 10; //Echelle horizontale par défaut
		this.pxy = 10; //Echelle verticale par défaut
	}
	
	/**
	 * Méthode permettant d'ajouter un objet à l'ArrayList listeObjet
	 * @param Objet a
	 */
	public void addObjet (Objet a){
		listeObjet.add(a);
	}
	
	/**
	 * Méthode permettant d'ajouter une lentille à l'ArrayList listeLentille
	 * @param LentilleMince a
	 */
	public void addLentille (LentilleMince a){
		listeLentille.add(a);
		Collections.sort(listeLentille);
	}
	
	/**
	 * Méthode permettant d'effacer tous les tracés
	 */
	public void effacerDessin(){
		listeObjet.clear();
		listeLentille.clear();
		repaint();
	}
	
	/**
	 * Méthode paint, celle qui dessine le tracé
	 */
	public void paint(Graphics g){
		
		//On définit un fond blanc
		
		g.setColor(Color.white); 
		g.fillRect(0,0,getWidth(),getHeight());
		
		//On définit la couleur de tracé en noir pour tracer les éléments "réels" : axe optique, lentille(s) et objet(s)
		
		g.setColor(Color.black);
	
		int pYAxeOpt = (int)(getHeight()/2); //Définit le centre selon y de la zone de dessin
		int tFleche = (int)(getHeight()*0.05); //Définit les dimensions du bout de la flèche
		
		//Dessin de l'axe optique
		
		g.drawLine(0,pYAxeOpt,getWidth(),pYAxeOpt);
		g.drawLine(getWidth(),pYAxeOpt,getWidth()-tFleche, pYAxeOpt+tFleche);
		g.drawLine(getWidth(),pYAxeOpt,getWidth()-tFleche, pYAxeOpt-tFleche);
	
		//Dessin des objets
		
		for(Objet o : listeObjet){ //Boucle qui parcourt la liste des objets
			if(o.infini == false){ //Ne trace que les objets qui ne sont pas à l'infini
				g.drawLine((int)(pxx*o.position),pYAxeOpt, (int)(pxx*o.position), pYAxeOpt-(int)(pxy*o.taille)); // Dessine l'objet comme une barre verticale
				
				// Donne les informations de l'objet (taille et position)
				
				if(o.taille>=0){ //Conditions pour savoir si les infos sont placées au-dessus ou en dessous de l'axe optique.
					g.drawString("x = " + o.position +"cm", (int)(pxx*o.position)-20,pYAxeOpt+15);
					g.drawString("AB = " + o.taille +"cm", (int)(pxx*o.position)-20,pYAxeOpt+30);
				} else {
					g.drawString("x = " + o.position +"cm", (int)(pxx*o.position)-20,pYAxeOpt-30);
					g.drawString("AB = " + o.taille +"cm", (int)(pxx*o.position)-20,pYAxeOpt-15);
				}
			}
		}
	
		//Dessin des lentilles
		
		for(int a = 0; a<listeLentille.size(); a++) { //Boucle qui parcourt la liste de lentilles
			LentilleMince l = listeLentille.get(a);	//On récupère la lentille
			g.drawLine((int)(pxx*l.position),(int)(pYAxeOpt-0.35*getHeight()), (int)(pxx*l.position), (int)(pYAxeOpt+0.35*getHeight())); // On trace le trait verticale de la lentille
			//On trace les infos de la lentille
			g.drawString("f'" + a + " = " + l.distanceFocale +"cm", (int)(pxx*l.position)-15,(int)(pYAxeOpt+0.40*getHeight()));
			g.drawString("x = " + l.position +"cm", (int)(pxx*l.position)-20,(int)(pYAxeOpt+0.40*getHeight()+15));
			
			if(l.distanceFocale<0) { //Condition pour savoir comment dessiner les bords du trait en fonction de la convergence ou de la divergence de la lentille
				g.drawLine((int)(pxx*l.position),(int)(pYAxeOpt-0.35*getHeight()), (int)(pxx*l.position-tFleche/2), (int)(pYAxeOpt-0.35*getHeight()-tFleche/2));
				g.drawLine((int)(pxx*l.position),(int)(pYAxeOpt-0.35*getHeight()), (int)(pxx*l.position+tFleche/2), (int)(pYAxeOpt-0.35*getHeight()-tFleche/2));
				g.drawLine((int)(pxx*l.position),(int)(pYAxeOpt+0.35*getHeight()), (int)(pxx*l.position+tFleche/2), (int)(pYAxeOpt+0.35*getHeight()+tFleche/2));
				g.drawLine((int)(pxx*l.position),(int)(pYAxeOpt+0.35*getHeight()), (int)(pxx*l.position-tFleche/2), (int)(pYAxeOpt+0.35*getHeight()+tFleche/2));
			} else { 
				g.drawLine((int)(pxx*l.position),(int)(pYAxeOpt-0.35*getHeight()), (int)(pxx*l.position-tFleche/2), (int)(pYAxeOpt-0.35*getHeight()+tFleche/2));
				g.drawLine((int)(pxx*l.position),(int)(pYAxeOpt-0.35*getHeight()), (int)(pxx*l.position+tFleche/2), (int)(pYAxeOpt-0.35*getHeight()+tFleche/2));
				g.drawLine((int)(pxx*l.position),(int)(pYAxeOpt+0.35*getHeight()), (int)(pxx*l.position+tFleche/2), (int)(pYAxeOpt+0.35*getHeight()-tFleche/2));
				g.drawLine((int)(pxx*l.position),(int)(pYAxeOpt+0.35*getHeight()), (int)(pxx*l.position-tFleche/2), (int)(pYAxeOpt+0.35*getHeight()-tFleche/2));
			}
		}
	
		//Tracé des rayons : trois rayons seront tracés à chaque fois, un qui passe par le haut de la lentille, un qui passe par le centre et un qui passe par le bas.
		
		Color rayon = new Color(255,0,0); //On définit la couleur des rayons réels
		Color virt = new Color(253,153,153); //On définit la couleur des rayons virtuels
		g.setColor(rayon); // On règle la couleur de tracé pour un rayon réel
		for(int i = 0; i<listeObjet.size(); i++){ //Boucle qui parcourt la liste des objets
			Objet a = new Objet();
			a = listeObjet.get(i); //On récupère l'objet souhaité
			for(int j = 0; j<listeLentille.size(); j++){ //Boucle qui parcourt le système optique
				LentilleMince l = listeLentille.get(j); //On récupère la lentille souhaitée
					if(a.infini == true){ //Si l'objet est infini
						if(l.distanceFocale>0) { //Si la distance focale > 0 alors les rayons sont réels
							int yDepartc; // coordonnées y de départ du rayon central
							int yDeparth; // coordonnées y de départ du rayon supérieur
							int yDepartb; // coordonnées y de départ du rayon inférieur
							int xDepart; // coordonnées x de départ de tous les rayons
							if( j == 0) { //Si c'est la première lentille (xDepart = 0)
								yDepartc = (int)((pYAxeOpt - (pxy*l.position*a.taille)));
								yDeparth = (int)((pYAxeOpt-0.35*getHeight() - (pxy*l.position*a.taille)));
								yDepartb = (int)((pYAxeOpt+0.35*getHeight() - (pxy*l.position*a.taille)));
								xDepart = 0;
							} else { //Sinon (xDepart = position de la lentille précédente)
								yDepartc = (int)((pYAxeOpt - (pxy*l.position*a.taille)));
								yDeparth = (int)((pYAxeOpt-0.35*getHeight() - (pxy*(listeLentille.get(i).position - l.position)*a.taille)));
								yDepartb = (int)((pYAxeOpt+0.35*getHeight() - (pxy*(listeLentille.get(i).position - l.position)*a.taille)));
								xDepart = (int)(pxx*listeLentille.get(i).position);
							}
							
							a = a.calculImage(l); //Calcul de l'image
							
							//Tracé du rayon central (ligne droite non déviée)
							
							g.drawLine(xDepart,yDepartc,(int)(pxx*a.position), pYAxeOpt-(int)(pxy*a.taille));
							
							//Tracé des rayons incidents à la lentille
							
							g.drawLine(0,yDeparth,(int)(pxx*l.position), (int)((pYAxeOpt-0.35*getHeight()))); //rayon supérieur
							g.drawLine(0,yDepartb,(int)(pxx*l.position), (int)((pYAxeOpt+0.35*getHeight()))); //rayon inférieur
							
							//Tracé des rayons sortants de la lentille
							 
							g.drawLine((int)(pxx*l.position), (int)((pYAxeOpt-0.35*getHeight())),(int)(pxx*a.position), pYAxeOpt-(int)(pxy*a.taille)); //rayon supérieur
							g.drawLine((int)(pxx*l.position), (int)((pYAxeOpt+0.35*getHeight())),(int)(pxx*a.position), pYAxeOpt-(int)(pxy*a.taille)); //rayon inférieur
						} else { //Distance focale <0 donc les rayons sortants sont virtuels
							int yDepartc; // coordonnées y de départ du rayon central
							int yDeparth; // coordonnées y de départ du rayon supérieur
							int yDepartb; // coordonnées y de départ du rayon inférieur
							int xDepart; // coordonnées x de départ de tous les rayons
							if( j == 0) { //Si c'est la première lentille (xDepart = 0)
								yDepartc = (int)((pYAxeOpt - (pxy*l.position*a.taille)));
								yDeparth = (int)((pYAxeOpt-0.35*getHeight() - (pxy*l.position*a.taille)));
								yDepartb = (int)((pYAxeOpt+0.35*getHeight() - (pxy*l.position*a.taille)));
								xDepart = 0;
							} else { //Sinon (xDepart = position de la lentille précédente)
								yDepartc = (int)((pYAxeOpt - (pxx*l.position*a.taille)));
								yDeparth = (int)((pYAxeOpt-0.35*getHeight() - (pxy*(listeLentille.get(i).position - l.position)*a.taille)));
								yDepartb = (int)((pYAxeOpt+0.35*getHeight() - (pxy*(listeLentille.get(i).position - l.position)*a.taille)));
								xDepart = (int)(pxx*listeLentille.get(i).position);
							}
							
							a = a.calculImage(l); //Calcul de l'image
							
							//Tracé du rayon centrale (ligne droite non déviée)
							
							g.drawLine(xDepart,yDepartc,(int)(pxx*l.position), pYAxeOpt);
							
							//Tracé des rayons incidents 
							
							g.drawLine(xDepart,yDeparth,(int)(pxx*l.position), (int)((pYAxeOpt-0.35*getHeight()))); //rayon supérieur
							g.drawLine(xDepart,yDepartb,(int)(pxx*l.position), (int)((pYAxeOpt+0.35*getHeight()))); //rayon inférieur
							
							
							//Tracé des rayons sortants
							g.setColor(virt); //Les rayons seront virtuels donc on change de couleur
							g.drawLine((int)(pxx*l.position), (int)((pYAxeOpt-0.35*getHeight())),(int)(pxx*a.position), pYAxeOpt-(int)(pxy*a.taille)); //rayon supérieur
							g.drawLine((int)(pxx*l.position), (int)((pYAxeOpt+0.35*getHeight())),(int)(pxx*a.position), pYAxeOpt-(int)(pxy*a.taille)); //rayon inférieur
							g.setColor(rayon); //On remet la couleur par défaut des rayons réels
						}
					} else {
						
						//Tracé des rayons incidents qui partent de l'extrémité de l'objet jusqu'à la lentille
						g.drawLine((int)(pxx*a.position),(int)(pYAxeOpt-pxy*a.taille),(int)(pxx*l.position), pYAxeOpt); // rayon central
						g.drawLine((int)(pxx*a.position),(int)(pYAxeOpt-pxy*a.taille),(int)(pxx*l.position), (int)((pYAxeOpt-0.35*getHeight()))); //rayon supérieur
						g.drawLine((int)(pxx*a.position),(int)(pYAxeOpt-pxy*a.taille),(int)(pxx*l.position), (int)((pYAxeOpt+0.35*getHeight()))); //rayon inférieur
						
						//Calcul de l'image
						a = a.calculImage(l);
						
						//Tracé des rayons sortants
						if(a.infini == true){ //Si l'image est à l'infini
							if(j+1 >= listeLentille.size()){//Si c'est la dernière lentille, le rayon va jusqu'au bord de la fenêtre
								g.drawLine((int)(pxx*l.position),pYAxeOpt, getWidth(), (int)((pYAxeOpt - (pxy*(getWidth()-l.position)*a.taille/pxx))));
								g.drawLine((int)(pxx*l.position), (int)((pYAxeOpt-0.35*getHeight())), getWidth(),(int)((pYAxeOpt-0.35*getHeight() - (pxy*(getWidth()-l.position)*a.taille/pxx))));
								g.drawLine((int)(pxx*l.position), (int)((pYAxeOpt+0.35*getHeight())), getWidth(),(int)((pYAxeOpt+0.35*getHeight() - (pxy*(getWidth()-l.position)*a.taille/pxx)))); 
							} else { //Sinon le rayon s'arrête au x de la lentille suivante
								LentilleMince lp = listeLentille.get(j+1);
								g.drawLine((int)(pxx*l.position),pYAxeOpt, (int)lp.position, (int)((pYAxeOpt - (pxy*(lp.position-l.position)*a.taille/pxx))));
								g.drawLine((int)(pxx*l.position), (int)((pYAxeOpt-0.35*getHeight())), (int)lp.position,(int)((pYAxeOpt-0.35*getHeight() - (pxy*(lp.position-l.position)*a.taille/pxx))));
								g.drawLine((int)(pxx*l.position), (int)((pYAxeOpt+0.35*getHeight())), (int)lp.position,(int)((pYAxeOpt+0.35*getHeight() - (pxy*(lp.position-l.position)*a.taille/pxx)))); 
							}
						} else { //Si l'image existe
							if(a.position<l.position){ //Si l'image est située avant la lentille (rayon virtuel)
								g.setColor(virt); //On passe la couleur pour les rayons virtuels
								g.drawLine((int)(pxx*l.position),pYAxeOpt, (int)(pxx*a.position), pYAxeOpt-(int)(pxy*a.taille));
								g.drawLine((int)(pxx*l.position), (int)((pYAxeOpt-0.35*getHeight())),(int)(pxx*a.position), pYAxeOpt-(int)(pxy*a.taille));
								g.drawLine((int)(pxx*l.position), (int)((pYAxeOpt+0.35*getHeight())),(int)(pxx*a.position), pYAxeOpt-(int)(pxy*a.taille));
								g.setColor(rayon); //On remet la couleur pour les rayons réels
							} else { //Si l'image est derrière la lentille les rayons sont réels
								g.drawLine((int)(pxx*l.position),pYAxeOpt, (int)(pxx*a.position), pYAxeOpt-(int)(pxy*a.taille));
								g.drawLine((int)(pxx*l.position), (int)((pYAxeOpt-0.35*getHeight())),(int)(pxx*a.position), pYAxeOpt-(int)(pxy*a.taille));
								g.drawLine((int)(pxx*l.position), (int)((pYAxeOpt+0.35*getHeight())),(int)(pxx*a.position), pYAxeOpt-(int)(pxy*a.taille));
							}
						}				
					}
					if(a.infini == false){ // Si l'image n'est pas à l'infini on trace l'image en bleu
						g.setColor(Color.blue); //On règle la couleur en bleu
						g.drawLine((int)(pxx*a.position),pYAxeOpt, (int)(pxx*a.position), pYAxeOpt-(int)(pxy*a.taille)); //On trace l'image
						
						// Donne les informations de l'objet (taille et position)
						
						if(a.taille>=0){ //Conditions pour savoir si les infos sont placées au-dessus ou en dessous de l'axe optique.
							g.drawString("x = " + a.position +"cm", (int)(pxx*a.position)-20,pYAxeOpt+15);
							g.drawString("AB = " + a.taille +"cm", (int)(pxx*a.position)-20,pYAxeOpt+30);
						} else {
							g.drawString("x = " + a.position +"cm", (int)(pxx*a.position)-20,pYAxeOpt-30);
							g.drawString("AB = " + a.taille +"cm", (int)(pxx*a.position)-20,pYAxeOpt-15);
						}
						g.setColor(rayon); //On remet la couleur pour les rayons réels
					}
			}
		}
	}
}
